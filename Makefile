INCS       ?= $(shell sdl2-config --cflags)
LIBS       ?= $(shell sdl2-config --libs)
OBJECTS    := e6809.o e8910.o osint.o vecx.o
ROMFILE    := rom.h
TARGET     := vecx
CFLAGS     ?= -O3 -Wall -Wextra
CFLAGS     += $(INCS)
CLEANFILES := $(TARGET) $(OBJECTS) $(ROMFILE)

all: $(ROMFILE) $(TARGET)

$(ROMFILE): rom.dat
	(echo 'static const unsigned char rom[] = { ' ;\
	 od -An -t x1 -v $< | \
	 sed -e 's/^ /0x/; s/$$/,/; s/ /,0x/g' ;\
	 echo '};') > $@

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

clean:
	$(RM) $(CLEANFILES)
