Vecx
====
Vectrex Emulator (Modified SDL2 port)

![Mine Storm II](screenshot.png)

Requirements
------------
* [libSDL2](https://www.libsdl.org)

Usage
-----
```sh
vecx [options] [cartfile]
Options:
  --help (-h)            Display this help message
  --alpha (-a) <0-255>   Set value for alpha channel
  --fullscreen (-f)      Launch in fullscreen mode
  --overlay (-o) <file>  Load overlay BMP file
  --height (-y) <pixels> Set window height in pixels
  --width (-x) <pixels>  Set window width in pixels
```
Use the arrow keys to control the direction. Keys 'a'/'s'/'d'/'f' correspond to
buttons 1/2/3/4 on the Vectrex joystick. Escape quits the emulator, 'o' toggles
display of overlay, 'p' pauses emulator, Shift-'r' resets the emulator and 'z'
toggles the fullscreen mode, '-' and '+' adjusts the brightness ...

The original games are freely available for non-commercial use ...

Authors
-------
* [Valavan Manohararajah](https://www.valavan.net/vectrex.html) - original author
* [John Hawthorn](https://github.com/jhawthorn/vecx) - SDL port
* [Nikita Zimin](https://twitter.com/nzeemin) - audio
* [Mateusz Muszynski](https://github.com/Timu5/vecx-SDL2) - SDL2 port
* [Simon Rodriguez](https://github.com/kosua20/vecx) - SDL2 port
* [Jens Guenther](https://gitlab.com/jengun/vecx) - Modified SDL2 port
