#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "osint.h"
#include "e8910.h"
#include "vecx.h"

#define EMU_TIMER 20 /* the emulators heart beats at 20 milliseconds */

static SDL_Window *window = NULL;
static SDL_Renderer *renderer= NULL;
static SDL_Texture *overlay = NULL;
static char *cartfilename = NULL;
static int alpha = 127;
static int show_overlay = 0;
static int window_height = 640;
static int window_width = 518;

void osint_render(void){
	int v;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, alpha);
	SDL_RenderFillRect(renderer, NULL);
	for(v = 0; v < vector_draw_cnt; v++){
		vector_t vector = vectors_draw[v];
		unsigned int  c = vector.color * 256 / VECTREX_COLORS;
		unsigned int x0 = vector.x0 / 32;
		unsigned int y0 = vector.y0 / 32;
		unsigned int x1 = vector.x1 / 32;
		unsigned int y1 = vector.y1 / 32;

		SDL_SetRenderDrawColor(renderer, c, c, c, alpha);
		if (x0 == x1 && y0 == y1) {
			SDL_RenderDrawLine(renderer, x0, y0, x0 + 1, y0);
			SDL_RenderDrawLine(renderer, x0, y0 + 1, x0 + 1, y0 + 1);
		}else{
			SDL_RenderDrawLine(renderer, x0, y0, x1, y1);
			SDL_RenderDrawLine(renderer, x0 + 1, y0 + 1, x1 + 1, y1 + 1);
		}
	}
	if(show_overlay){
		if(overlay)
			SDL_RenderCopy(renderer, overlay, NULL, NULL);
	}
	SDL_RenderPresent(renderer);
}

static void init(void){
	memset(cart, 0, sizeof (cart));
	if(cartfilename){
		FILE *f;
		if(!(f = fopen(cartfilename, "rb"))){
			perror(cartfilename);
			exit(EXIT_FAILURE);
		}
		fread(cart, 1, sizeof (cart), f);
		fclose(f);
	}
}

static void quit(int ret){
	if(renderer)
		SDL_DestroyRenderer(renderer);
	if(window)
		SDL_DestroyWindow(window);

	SDL_Quit();
	exit(ret);
}

static void readevents(void){
	SDL_Event e;
	while(SDL_PollEvent(&e)){
		switch(e.type){
			case SDL_QUIT:
				quit(EXIT_SUCCESS);
				break;
			case SDL_KEYDOWN:
				switch(e.key.keysym.sym){
					case SDLK_ESCAPE:
						exit(EXIT_SUCCESS);
					case SDLK_a:
						snd_regs[14] &= ~0x01;
						break;
					case SDLK_s:
						snd_regs[14] &= ~0x02;
						break;
					case SDLK_d:
						snd_regs[14] &= ~0x04;
						break;
					case SDLK_f:
						snd_regs[14] &= ~0x08;
						break;
					case SDLK_LEFT:
						alg_jch0 = 0x00;
						break;
					case SDLK_RIGHT:
						alg_jch0 = 0xff;
						break;
					case SDLK_UP:
						alg_jch1 = 0xff;
						break;
					case SDLK_DOWN:
						alg_jch1 = 0x00;
						break;
					case SDLK_PLUS:
						if (alpha < 255)
							alpha++;
						break;
					case SDLK_MINUS:
						if (alpha > 0)
							alpha--;
						break;
					case SDLK_o:
						show_overlay = !show_overlay;
						break;
					case SDLK_p:
						SDL_SetWindowTitle(window, "Vecx PAUSED");
						SDL_PauseAudio(1);
						while(SDL_WaitEvent(&e)){
							if(e.type == SDL_KEYDOWN){
								SDL_SetWindowTitle(window, "Vecx");
								SDL_PauseAudio(0);
								return;
							}
							SDL_SetRenderDrawColor(renderer, 0, 0, 0, alpha);
							SDL_RenderClear(renderer);
							osint_render();
						}
						break;
					case SDLK_r:
						if(SDL_GetModState() & KMOD_SHIFT)
							vecx_reset();
						break;
					default:
						break;
				}
				break;
			case SDL_KEYUP:
				switch(e.key.keysym.sym){
					case SDLK_a:
						snd_regs[14] |= 0x01;
						break;
					case SDLK_s:
						snd_regs[14] |= 0x02;
						break;
					case SDLK_d:
						snd_regs[14] |= 0x04;
						break;
					case SDLK_f:
						snd_regs[14] |= 0x08;
						break;
					case SDLK_LEFT:
						alg_jch0 = 0x80;
						break;
					case SDLK_RIGHT:
						alg_jch0 = 0x80;
						break;
					case SDLK_UP:
						alg_jch1 = 0x80;
						break;
					case SDLK_DOWN:
						alg_jch1 = 0x80;
						break;
					default:
						break;
				}
				break;
			case SDL_WINDOWEVENT:
				if(e.window.event == SDL_WINDOWEVENT_EXPOSED){
					SDL_SetRenderDrawColor(renderer, 0, 0, 0, alpha);
					SDL_RenderClear(renderer);
				}
				break;
			default:
				break;
		}
	}

	if (e.key.keysym.sym == SDLK_z){
		int flags = (SDL_GetWindowFlags(window) ^ SDL_WINDOW_FULLSCREEN_DESKTOP);

		SDL_SetWindowFullscreen(window, flags);
		SDL_ShowCursor((flags & SDL_WINDOW_FULLSCREEN_DESKTOP) ? SDL_DISABLE : SDL_ENABLE);
	}
}

static void osint_emuloop(void){
	Uint32 next_time = SDL_GetTicks() + EMU_TIMER;
	vecx_reset();
	for(;;){
		vecx_emu((VECTREX_MHZ / 1000) * EMU_TIMER);
		readevents();

		{
			Uint32 now = SDL_GetTicks();
			if(now < next_time)
				SDL_Delay(next_time - now);
			else
				next_time = now;
			next_time += EMU_TIMER;
		}
	}
}

static void load_overlay(const char *filename){
	SDL_Surface *image = SDL_LoadBMP(filename);

	if(image){
		overlay = SDL_CreateTextureFromSurface(renderer, image);
		show_overlay = 1;
		SDL_FreeSurface(image);
	}else{
		fprintf(stderr, "SDL_LoadBMP: %s\n", SDL_GetError());
	}
}

int main(int argc, char *argv[]){
	int i;

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0){
		fprintf(stderr, "Failed to initialize SDL: %s\n", SDL_GetError());
		quit(-1);
	}
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
	SDL_CreateWindowAndRenderer(518, 640, SDL_WINDOW_RESIZABLE, &window, &renderer);
	if(window == NULL || renderer == NULL){
		fprintf(stderr, "Failed to initialize SDL window/renderer: %s\n", SDL_GetError());
		quit(-2);
	}
	SDL_RenderSetLogicalSize(renderer, 518 * 2, 640 * 2);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	for(i = 1; i < argc; i++){
		if(strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0){
			puts("Usage: vecx [options] [cartfile]");
			puts("Options:");
			puts("  --help (-h)            Display this help message");
			puts("  --alpha (-a) <0-255>   Set value for alpha channel");
			puts("  --fullscreen (-f)      Launch in fullscreen mode");
			puts("  --overlay (-o) <file>  Load overlay BMP file");
			puts("  --height (-y) <pixels> Set window height in pixels");
			puts("  --width (-x) <pixels>  Set window width in pixels");
			quit(0);
		}
		else if(strcmp(argv[i], "--alpha") == 0 || strcmp(argv[i], "-a") == 0){
			alpha = atoi(argv[++i]) & 255;
		}
		else if(strcmp(argv[i], "--fullscreen") == 0 || strcmp(argv[i], "-f") == 0){
			SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
			SDL_ShowCursor(SDL_DISABLE);
		}
		else if(strcmp(argv[i], "--overlay") == 0 || strcmp(argv[i], "-o") == 0){
			load_overlay(argv[++i]);
		}
		else if(strcmp(argv[i], "--height") == 0 || strcmp(argv[i], "-y") == 0){
			window_height = atoi(argv[++i]);
		}
		else if(strcmp(argv[i], "--width") == 0 || strcmp(argv[i], "-x") == 0){
			window_width = atoi(argv[++i]);
		}
		else if(i == argc - 1){
			cartfilename = argv[i];
		}
		else{
			fprintf(stderr, "Unknown option: %s\n", argv[i]);
			quit(0);
		}
	}
	SDL_SetWindowTitle(window, "Vecx");
	SDL_SetWindowSize(window, window_width, window_height);

	init();

	e8910_init_sound();
	osint_emuloop();
	e8910_done_sound();

	quit(0);
}

